// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import Vuetify from "vuetify";
// import colors from 'vuetify/es5/util/colors'
import VueSweetalert2 from 'vue-sweetalert2';
import "vuetify/dist/vuetify.min.css";
import "./assets/scss/style.scss";
// import axios from "axios";
import store from "@/store/index";
import Chart from "chart.js";

Vue.use(Vuetify, {
  theme: {
    primary: "#764ba2",
    secondary: "#667eea",
    // accent: "#8c9eff",
    error: "#dc3545"
  }
});

Vue.use(VueSweetalert2);

Vue.filter("dinheiro", valor => {
  return `R$ ${parseFloat(valor).toFixed(2)}`.replace(".", ",");
});

Vue.filter("formatDate", dateToFormat => {
  var t = dateToFormat.split(/[- :]/);
  var dateFormated = t[2] + "/" + t[1] + "/" + t[0];
  return dateFormated;
});

Vue.filter("formatDateToBD", dateToFormat => {
  var t = dateToFormat.split(/[/ :]/);
  var dateFormated = t[2] + "-" + t[1] + "-" + t[0];
  return dateFormated;
});

Vue.filter("timestampConverter", UNIX_timestamp => { 
  var a = new Date(UNIX_timestamp * 1000);
  // i dont need it
  // var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  
  var year = a.getFullYear();
  // var month = months[a.getMonth()];
  var month = a.getMonth()+1;
  var date = a.getDate();
  var time = year + '-' + month + '-' + date ;
  return time;
});

// Vue.prototype.$https = axios;
// Vue.prototype.$urlAPI = "http://localhost:8000";

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  store,
  el: "#app",
  router,
  components: { App },
  template: "<App/>"
});
