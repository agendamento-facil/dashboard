import Vue from "vue";
import Vuex from "vuex";
import firebase from "../firebase-config";
import { db } from "../firebase-config";

import customers from "./modules/customers";

// Pega todos os módulos definidos dentro do arquivo getters
// import * as getters from "./getters";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    status: null,
    error: null
  },
  getters: {
    user(state) {
      return state.user;
    },
    status(state) {
      return state.status;
    },
    error(state) {
      return state.error;
    }
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
    removeUser(state, payload) {
      state.user = null;
    },
    setStatus(state, payload) {
      state.status = payload;
    },
    setError(state, payload) {
      state.error = payload;
    }
  },
  actions: {
    signUpAction({ commit }, payload) {
      commit("setStatus", "loading");

      firebase
        .auth()
        .createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          response.user.updateProfile({
            displayName: payload.displayName,
            photoURL:
              "https://cdn5.vectorstock.com/i/1000x1000/45/79/male-avatar-profile-picture-silhouette-light-vector-4684579.jpg"
          });

          // var userInfo = {
          //   displayName: responseUpdated.displayName,
          //   email: response.user.email,
          //   uid: response.user.uid,
          //   photoURL: response.user.photoURL
          // };

          // commit("setUser", userInfo);

          db.collection("userInformations")
            .doc(response.user.uid)
            .set({
              company: payload.company,
              cnpj: payload.cnpj,
              companyCategory: payload.companyCategory
            })
            .then(function() {
              commit("setStatus", "success");
              commit("setError", null);
            })
            .catch(function(error) {
              console.error("Error writing document: ", error);
            });

        })
        .catch(error => {
          commit("setStatus", "failure");
          commit("setError", error.message);
        });
    },
    signUpFacebookAction({ commit }, payload) {
      var provider = new firebase.auth.FacebookAuthProvider();
      firebase
        .auth()
        .signInWithPopup(provider)
        .then(function(result) {
          // This gives you a Facebook Access Token. You can use it to access the Facebook API.
          var token = result.credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          console.log(token, user);
          // ...
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });
    },
    signInAction({ commit }, payload) {
      commit("setStatus", "loading");
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          var userInfo = {
            displayName: response.user.displayName,
            email: response.user.email,
            uid: response.user.uid,
            photoURL: response.user.photoURL
          };
          commit("setUser", userInfo);
          commit("setStatus", "success");
          commit("setError", null);

          if (payload.$route.path == "/") {
            payload.$router.push("/admin/dashboard");
          }
        })
        .catch(error => {
          commit("setStatus", "failure");
          commit("setError", error.message);
        });
    },
    checkSignInAction({ commit }, payload) {
      firebase.auth().onAuthStateChanged(function(user) {
        commit("setStatus", "loading");
        if (user) {
          var userInfo = {
            displayName: user.displayName,
            email: user.email,
            uid: user.uid,
            photoURL: user.photoURL
          };
          commit("setUser", userInfo);
          commit("setStatus", "success");
          commit("setError", null);
          if (
            payload.$route.path == "/" ||
            payload.$route.path == "/admin" ||
            payload.$route.path == "/admin/"
          ) {
            payload.$router.push("/admin/dashboard");
          }
        } else {
          commit("setStatus", "failure");
          commit("setError", "Ocorreu um erro ao realizar o login.");
          payload.$router.push("/");
        }
      });
    },
    signOutAction({ commit }) {
      firebase
        .auth()
        .signOut()
        .then(response => {
          commit("setUser", null);
          commit("setStatus", "success");
          commit("setError", null);
        })
        .catch(error => {
          commit("setStatus", "failure");
          commit("setError", error.message);
        });
    }
  },
  modules: { customers }
});
