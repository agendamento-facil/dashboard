import { db } from "../../firebase-config";

export default {
  state: {
    customersList: null,
    customersQuantity: null
  },
  getters: {
    customersList(state) {
      return state.customersList;
    },
    CustomersQuantity(state) {
      return state.customersQuantity;
    }
  },
  mutations: {
    setCustomersList(state, payload) {
      state.customersList = payload;
    },
    setCustomersQuantity(state, payload) {
      state.customersQuantity = payload;
    }
  },
  actions: {
    getCustomersList({ commit }, payload) {
      db.collection("customers").doc(payload).onSnapshot(function(doc) {
        // console.log("Current data: ", Object.keys(doc.data()).length, "doc:", doc);
        if (doc.exists) {
          if (Object.keys(doc.data()) != 0) {
            commit("setCustomersList", doc.data());
            commit("setCustomersQuantity", Object.keys(doc.data()).length);
          } else {
            commit("setCustomersList", null);
            commit("setCustomersQuantity", 0);
          }
          
        } else {
          commit("setCustomersList", null);
          commit("setCustomersQuantity", 0);
        }
    });   
    },
    setNewCustomerAction({ commit }, payload) {
      var idTimestamp = new Date().getTime();
      db.collection("customers").doc(payload.newCustomer.userId).set({
        [idTimestamp]: {
          employee_id: 0,
          id: new Date().getTime(),
          name: payload.newCustomer.name,
          email: payload.newCustomer.email,
          phone: payload.newCustomer.phone,
          created_at: new Date(),
          updated_at: new Date()
        }
      }, { merge: true })
      .then(function() {
          payload.dialog = false;
          payload.$swal('Adicionado com sucesso', 'O cliente foi adicionado com sucesso.', 'success');
      })
      .catch(function(error) {
          console.error("Error writing document: ", error);
      });
    }
  }
};
