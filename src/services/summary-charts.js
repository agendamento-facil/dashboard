export const dashboardChartData = {
  type: "line",
  data: {
    labels: [
      "Segunda-feira",
      "Terça-feira",
      "Quarta-feira",
      "Quinta-feira",
      "Sexta-feira",
      "Sábado",
      "Domingo"
    ],
    datasets: [
      {
        // one line graph
        label: "Agendamentos",
        data: [7, 9, 10, 8, 16, 13, 14],
        fill: false,
        borderColor: "#764ba2",
        borderWidth: 3
      },
      // {
      //   // another line graph
      //   label: "Planet Mass (x1,000 km)",
      //   data: [4.8, 12.1, 12.7, 6.7, 139.8, 116.4, 50.7, 49.2],
      //   backgroundColor: [
      //     "rgba(71, 183,132,.5)" // Green
      //   ],
      //   borderColor: ["#47b784"],
      //   borderWidth: 3
      // }
    ]
  },
  options: {
    responsive: true,
    lineTension: 1,
    scales: {
      yAxes: [
        {
          ticks: {
            // beginAtZero: true,
            padding: 25
          }
        }
      ]
    }
  }
};

export default dashboardChartData;
