import firebase from "firebase"; // For all application
require("firebase/firestore");

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDV6Anl9Ea7Pn-YKxJgvYJ4FMKMmnHwZzM",
  authDomain: "agendamento-facil.firebaseapp.com",
  databaseURL: "https://agendamento-facil.firebaseio.com",
  projectId: "agendamento-facil",
  storageBucket: "agendamento-facil.appspot.com",
  messagingSenderId: "362999026566",
  appId: "1:362999026566:web:2148b7def3495d7f"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
export { db };

export default firebase;
