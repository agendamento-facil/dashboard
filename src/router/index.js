import Vue from "vue";
import Router from "vue-router";
import Authentication from "@/views/Auth";
import Admin from "@/views/Admin";
import Dashboard from "../views/Admin/Dashboard.vue";
import Calendar from "../views/Admin/Calendar.vue";
import Customers from "../views/Admin/Customers.vue";
import Services from "../views/Admin/Services.vue";
import Products from "../views/Admin/Products.vue";
import Marketing from "../views/Admin/Marketing.vue";
import Reports from "../views/Admin/Reports.vue";
import Settings from "../views/Admin/Services.vue";

Vue.use(Router);

export default new Router({
  // mode: "history",
  routes: [
    {
      path: "/",
      name: "Authentication",
      component: Authentication
    },
    {
      path: "/admin",
      name: "Admin",
      component: Admin,
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard
        },
        {
          path: "calendar",
          name: "Calendar",
          component: Calendar
        },
        {
          path: "customers",
          name: "Customers",
          component: Customers
        },
        {
          path: "services",
          name: "Services",
          component: Services
        },
        {
          path: "products",
          name: "Products",
          component: Products
        },
        {
          path: "marketing",
          name: "Marketing",
          component: Marketing
        },
        {
          path: "reports",
          name: "Reports",
          component: Reports
        },
        {
          path: "settings",
          name: "Settings",
          component: Settings
        }
      ]
    }
  ]
});
